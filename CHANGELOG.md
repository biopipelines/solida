# Change Log

## [0.6.2] - 2017-12-19
### Changed
- bugfix

## [0.6.2] - 2017-12-19
### Changed
- By default, set the project name same as the profile name.
- Ride deployment fixed.

## [0.6.1] - 2017-12-11
### Changed
- Notification handler moved to specific rule.

## [0.6.0] - 2017-11-27
### Changed
- Several bugs of the launcher have been fixed.

## [0.5.9] - 2017-11-27
### Added
- Ansible setup for ride.

## [0.5.8] - 2017-11-17
### Changed
- run.project.sh launcher can accept  different Snakefiles (see: ./run.project.sh -h).
- In the Solida profile now can be specified the number of jobs to submit.
